﻿// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ОбработчикиСобытий

// Предопределенный метод
// 
Процедура УстановкаПараметровСеанса(ИменаПараметровСеанса)
	
	// Подсистема события ПослеЗавершенияТранзакции
	Если ИменаПараметровСеанса = Неопределено Тогда
		пзт_Подсистема.Инициализировать();
	КонецЕсли;
	// Конец Подсистема события ПослеЗавершенияТранзакции
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-oт