﻿// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ОписаниеПеременных

#КонецОбласти


#Область ОбработчикиСобытийФормы

#КонецОбласти


#Область ОбработчикиКомандФормы

#КонецОбласти 


#Область ОбработчикиСобытийЭлементовШапкиФормы

// По расшифровке отчета. Открываем объект/набор записей
// 
&НаКлиенте
Процедура РезультатОбработкаРасшифровки(Элемент, Расшифровка, СтандартнаяОбработка)
	
	СтандартнаяОбработка	= Ложь;
	
КонецПроцедуры

#КонецОбласти 


#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on